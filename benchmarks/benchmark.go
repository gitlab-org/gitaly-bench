package benchmarks

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/pinterest/bender"
	"github.com/pinterest/bender/hist"
	"gitlab.com/gitlab-org/gitaly-bench/operations"
)

// RunBenchmark will execute the benchmark
func RunBenchmark(name string, goroutines int, iterations int, operation operations.BenchmarkOperation) {
	h := hist.NewHistogram(1000, int(60*time.Second))

	recorder := make(chan interface{}, 128)

	go bender.Record(recorder, bender.NewHistogramRecorder(h))

	log.Printf("Running %v. Iterations %v, concurrency %v", name, iterations, goroutines)

	var wg sync.WaitGroup

	start := time.Now().UnixNano()
	recorder <- &bender.StartEvent{Start: start}

	for i := 1; i <= goroutines; i++ {
		wg.Add(1)

		go func() {
			defer wg.Done()

			for i := 1; i <= iterations; i++ {
				start := time.Now().UnixNano()
				recorder <- &bender.StartRequestEvent{Time: start, Request: nil}
				err := operation.Run()
				recorder <- &bender.EndRequestEvent{Start: start, End: time.Now().UnixNano(), Response: nil, Err: err}

			}
		}()
	}
	wg.Wait()
	recorder <- &bender.EndEvent{Start: start, End: time.Now().UnixNano()}

	fmt.Println(h)
}
