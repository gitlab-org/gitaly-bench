package operations

import (
	"context"
	"flag"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

type gc struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.RepositoryServiceClient
	f          func(gc) error
}

// GCFlagSet are flags used by GC and Repack
var GCFlagSet = flag.NewFlagSet("gc", flag.ExitOnError)
var bitmapFlag = GCFlagSet.Bool("bitmap", false, "Create bitmap")

func (b gc) Run() error {
	return b.f(b)
}

func garbageCollect(b gc) error {
	request := &pb.GarbageCollectRequest{
		Repository:   b.repository,
		CreateBitmap: *bitmapFlag,
	}

	_, err := b.client.GarbageCollect(context.Background(), request)

	return err
}

func repackFullBenchmark(b gc) error {
	request := &pb.RepackFullRequest{
		Repository:   b.repository,
		CreateBitmap: *bitmapFlag,
	}

	_, err := b.client.RepackFull(context.Background(), request)

	return err
}

// NewGC creates a new benchmark operation
func NewGC(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewRepositoryServiceClient(conn)

	return gc{repository: repository, conn: conn, client: client, f: garbageCollect}
}

// NewRepackFull creates a new benchmark operation
func NewRepackFull(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewRepositoryServiceClient(conn)
	return gc{repository: repository, conn: conn, client: client, f: repackFullBenchmark}
}
