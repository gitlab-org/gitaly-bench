module gitlab.com/gitlab-org/gitaly-bench

go 1.13

require (
	github.com/gogo/protobuf v1.3.0 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20180522105215-e9c5d9645c43 // indirect
	github.com/pinterest/bender v0.0.0-20161010143647-dc46ca79d9a8
	github.com/pkg/profile v1.2.1
	github.com/stretchr/testify v1.4.0 // indirect
	gitlab.com/gitlab-org/gitaly v0.133.1-0.20181121142027-0cb101c5d6af
	gitlab.com/gitlab-org/gitaly-proto v0.123.0
	golang.org/x/lint v0.0.0-20190909230951-414d861bb4ac // indirect
	golang.org/x/tools v0.0.0-20190911230505-6bfd74cf029c // indirect
	google.golang.org/grpc v1.16.0
)
