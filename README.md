# Gitaly Benchmarking Tool

### Usage

```
gitaly-bench --host unix:///Users/andrewn/code/gitlab/gitlab-development-kit/gitaly.socket -repo moogroup/unicorns-are-awesome.git -concurrent 8 -iterations 10000
```


### Options
```
$ gitaly-bench
Usage of gitaly-bench [options] command:

Options:
  -concurrency int
    	Number of concurrent threads to test the benchmark on (default 10)
  -host string
    	The server address in the format of host:port (default "tcp://127.0.0.1:9999")
  -iterations int
    	Number of iterations to perform on each thread (default 10)
  -profile block
    	Profiling mode: block / `cpu` / `mem` / `mutex`
  -repo string
    	Relative path of git repo within storage
  -storage string
    	The storage to test (default "default")

`command` being one of:
	find-all-branch-names
	find-all-branches
	info-refs
	find-commit
	gc
	commit-diff
	has-local-branches
	repack-full
Use gitaly-bench command --help for command options
```

### TODO

Add more benchmarking operations. Currently only a small number of operations are supported.

### LICENSE

```
The MIT License (MIT)

Copyright (c) 2016-2017 GitLab B.V.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
