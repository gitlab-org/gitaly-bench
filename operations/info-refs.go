package operations

import (
	"context"
	"io"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

type infoRefs struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.SmartHTTPServiceClient
}

func (b *infoRefs) Run() error {
	request := &pb.InfoRefsRequest{
		Repository: b.repository,
	}

	resp, err := b.client.InfoRefsUploadPack(context.Background(), request)
	if err != nil {
		return err
	}

	for err == nil {
		_, err = resp.Recv()
	}
	if err == io.EOF {
		err = nil
	}

	return err
}

// NewInfoRefs creates a new benchmark operation
func NewInfoRefs(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewSmartHTTPServiceClient(conn)

	return &infoRefs{repository: repository, conn: conn, client: client}
}
