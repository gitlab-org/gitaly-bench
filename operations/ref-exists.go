package operations

import (
	"context"
	"flag"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

// RefExistsFlagSet are flags used by RefExists
var RefExistsFlagSet = flag.NewFlagSet("refex", flag.ExitOnError)
var refExistsRef = RefExistsFlagSet.String("ref", "", "Ref")

type refex struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.RefServiceClient
	f          func(refex) error
}

func (b refex) Run() error {
	return b.f(b)
}

func refExistsBenchmark(b refex) error {
	request := &pb.RefExistsRequest{
		Repository: b.repository,
		Ref:        []byte(*refExistsRef),
	}

	_, err := b.client.RefExists(context.Background(), request)
	return err
}

// NewRefExists create a new benchmark operation
func NewRefExists(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewRefServiceClient(conn)

	return refex{repository: repository, conn: conn, client: client, f: refExistsBenchmark}
}
