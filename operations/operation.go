package operations

// BenchmarkOperation is an operation that will be benchmarked. Implementation
// should be threadsafe
type BenchmarkOperation interface {

	// Run a single instance of the benchmark
	Run() error
}
