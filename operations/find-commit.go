package operations

import (
	"context"
	"flag"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

type cb struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.CommitServiceClient
	f          func(cb) error
}

func (b cb) Run() error {
	return b.f(b)
}

var (
	// FindCommitFlagSet allows for FindCommit specific benchmarking options
	FindCommitFlagSet  = flag.NewFlagSet("fc", flag.ExitOnError)
	findCommitRevision = FindCommitFlagSet.String("revision", "HEAD", "Revision, or commit id to look up")
)

func findCommitBenchmark(b cb) error {
	request := &pb.FindCommitRequest{
		Repository: b.repository,
		Revision:   []byte(*findCommitRevision),
	}

	_, err := b.client.FindCommit(context.Background(), request)
	if err != nil {
		return err
	}

	return nil
}

// NewFindCommit returns a benchmark operation
func NewFindCommit(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewCommitServiceClient(conn)

	return cb{repository: repository, conn: conn, client: client, f: findCommitBenchmark}
}
