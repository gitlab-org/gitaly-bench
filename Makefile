PREFIX := /usr/local
PKG := gitlab.com/gitlab-org/gitaly-bench
TARGET_DIR := $(CURDIR)/_build
TARGET_SETUP := $(TARGET_DIR)/.ok
BIN_BUILD_DIR := $(TARGET_DIR)/bin
PKG_BUILD_DIR := $(TARGET_DIR)/src/$(PKG)
COVERAGE_DIR := $(TARGET_DIR)/cover

export GOPATH := $(TARGET_DIR)
export PATH := $(GOPATH)/bin:$(PATH)

# Returns a list of all non-vendored (local packages)
LOCAL_PACKAGES = $(shell cd "$(PKG_BUILD_DIR)" && GOPATH=$(GOPATH) go list ./... | grep -v '^$(PKG)/vendor/')
LOCAL_GO_FILES = $(shell find . -name '*.go' -not -path "./vendor/*" -not -path "./_build/*")

# Developer Tools
GOLINT = $(BIN_BUILD_DIR)/golint
GOIMPORTS = $(BIN_BUILD_DIR)/goimports

.PHONY: all
all: build

$(TARGET_SETUP):
	rm -rf $(TARGET_DIR)
	mkdir -p "$(dir $(PKG_BUILD_DIR))"
	ln -sf ../../../.. "$(PKG_BUILD_DIR)"
	mkdir -p "$(BIN_BUILD_DIR)"
	touch "$(TARGET_SETUP)"

.PHONY: build
build: $(TARGET_SETUP)
	go build $(PKG_BUILD_DIR)/gitaly-bench.go
	GOOS=linux GOARCH=amd64 go build -o gitaly-bench-linux-amd64 $(PKG_BUILD_DIR)/gitaly-bench.go

build-local: $(TARGET_SETUP)
	go build $(PKG_BUILD_DIR)/gitaly-bench.go

.PHONY: verify
verify: lint check-formatting

.PHONY: test
test: $(TARGET_SETUP)
	@go test $(LOCAL_PACKAGES)

.PHONY: lint
lint: $(GOLINT)
	$(GOLINT) -set_exit_status $(LOCAL_PACKAGES)

.PHONY: check-formatting
check-formatting: $(TARGET_SETUP) $(GOIMPORTS)
	@test -z "$$($(GOIMPORTS) -e -l $(LOCAL_GO_FILES))" || (echo >&2 "Formatting or imports need fixing: 'make format'" && $(GOIMPORTS) -e -l $(LOCAL_GO_FILES) && false)

.PHONY: format
format: $(TARGET_SETUP) $(GOIMPORTS)
	# In addition to fixing imports, goimports also formats your code in the same style as gofmt
	# so it can be used as a replacement.
	$(GOIMPORTS) -w -l $(LOCAL_GO_FILES)

.PHONY: clean
clean:
	rm -rf $(TARGET_DIR)

# Install golint
$(GOLINT): $(TARGET_SETUP)
	go get -v golang.org/x/lint/golint

# Install goimports
$(GOIMPORTS): $(TARGET_SETUP)
	go get -v golang.org/x/tools/cmd/goimports
