package operations

import (
	"context"
	"flag"
	"io"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

type cd struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.DiffServiceClient
	f          func(cd) error
}

// CommitDiffFlagSet are flags used by CommitDiff
var CommitDiffFlagSet = flag.NewFlagSet("cd", flag.ExitOnError)
var commitDiffLeftCommitID = CommitDiffFlagSet.String("left", "", "LeftCommitID")
var commitDiffRightCommitID = CommitDiffFlagSet.String("right", "", "RightCommitID")
var commitDiffIgnoreWhitespaceChange = CommitDiffFlagSet.Bool("ignore-whitespace", true, "IgnoreWhitespaceChange")
var commitDiffCollapseDiffs = CommitDiffFlagSet.Bool("collapse-diffs", true, "CollapseDiffs")
var commitDiffEnforceLimits = CommitDiffFlagSet.Bool("enforce-limits", true, "EnforceLimits")
var commitDiffMaxFiles = CommitDiffFlagSet.Int("max-files", 0, "MaxFiles")
var commitDiffMaxLines = CommitDiffFlagSet.Int("max-lines", 0, "MaxLines")
var commitDiffMaxBytes = CommitDiffFlagSet.Int("max-bytes", 0, "MaxBytes")
var commitDiffSafeMaxFiles = CommitDiffFlagSet.Int("safe-max-files", 0, "SafeMaxFiles")
var commitDiffSafeMaxLines = CommitDiffFlagSet.Int("safe-max-lines", 0, "SafeMaxLines")
var commitDiffSafeMaxBytes = CommitDiffFlagSet.Int("safe-max-bytes", 0, "SafeMaxBytes")

func (b cd) Run() error {
	return b.f(b)
}

func commitDiff(b cd) error {
	request := &pb.CommitDiffRequest{
		Repository:             b.repository,
		LeftCommitId:           *commitDiffLeftCommitID,
		RightCommitId:          *commitDiffRightCommitID,
		IgnoreWhitespaceChange: *commitDiffIgnoreWhitespaceChange,
		Paths:                  nil,
		CollapseDiffs:          *commitDiffCollapseDiffs,
		EnforceLimits:          *commitDiffEnforceLimits,
		MaxFiles:               int32(*commitDiffMaxFiles),
		MaxLines:               int32(*commitDiffMaxLines),
		MaxBytes:               int32(*commitDiffMaxBytes),
		SafeMaxFiles:           int32(*commitDiffSafeMaxFiles),
		SafeMaxLines:           int32(*commitDiffSafeMaxLines),
		SafeMaxBytes:           int32(*commitDiffSafeMaxBytes),
	}

	stream, err := b.client.CommitDiff(context.Background(), request)
	if err != nil {
		return err
	}

	for {
		_, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
	}

}

// NewCommitDiff creates a new benchmark operation
func NewCommitDiff(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewDiffServiceClient(conn)
	return cd{repository: repository, conn: conn, client: client, f: commitDiff}
}
