package operations

import (
	"context"

	"google.golang.org/grpc"

	pb "gitlab.com/gitlab-org/gitaly-proto/go/gitalypb"
)

type hlb struct {
	repository *pb.Repository
	conn       *grpc.ClientConn
	client     pb.RepositoryServiceClient
	f          func(hlb) error
}

func (b hlb) Run() error {
	return b.f(b)
}

func hasLocalBranches(b hlb) error {
	request := &pb.HasLocalBranchesRequest{
		Repository: b.repository,
	}

	_, err := b.client.HasLocalBranches(context.Background(), request)

	return err
}

// NewHasLocalBranches creates a new benchmark operation
func NewHasLocalBranches(repository *pb.Repository, conn *grpc.ClientConn) BenchmarkOperation {
	client := pb.NewRepositoryServiceClient(conn)

	return hlb{repository: repository, conn: conn, client: client, f: hasLocalBranches}
}
